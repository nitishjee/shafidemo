package comp.shrofile.mycallingapp;

import android.telecom.Call;
import android.telecom.InCallService;
import android.util.Log;

public class CallService extends InCallService {

    @Override
    public void onCallAdded(Call call) {
        super.onCallAdded(call);

        if (OngoingCall.calls.size() == 0) {
            new OngoingCall().setCall(call);
            CallActivity.start(this, call);
        } else {
            new OngoingCall().setCall(call);
        }
        Log.d("nitish", call + "add");
    }

    @Override
    public void onCallRemoved(Call call) {
        super.onCallRemoved(call);

        OngoingCall.calls.remove(call);
        if (OngoingCall.calls.size() < 3) {
            new OngoingCall().disconnectAll();
            CallActivity callActivity = (CallActivity) CallApp.getInstance().getActivity();
            callActivity.callDisconnted();
        }
        Log.d("nitish", "Remove" + call);

    }
}

