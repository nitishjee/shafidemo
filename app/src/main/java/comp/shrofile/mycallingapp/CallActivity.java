package comp.shrofile.mycallingapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.telecom.Call;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import kotlin.collections.CollectionsKt;

import static comp.shrofile.mycallingapp.Constants.asString;

public class CallActivity extends AppCompatActivity {

    private TextView callInfo;
    private CompositeDisposable disposables;
    private OngoingCall ongoingCall;
    public static String path1 = "";
    private boolean aswCalled = false;
    /*  private final CompositeDisposable compositeDisposable = new CompositeDisposable();
      private final ApiService apiService = RetrofitClient.getInstance();*/
    private boolean serviceCalled = false;
    AudioManager audioManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);

        CallApp.getInstance().setActivity(this);
        callInfo = findViewById(R.id.callInfo);
        ongoingCall = new OngoingCall();
        disposables = new CompositeDisposable();
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

    }

    @Override
    protected void onStart() {
        super.onStart();

        assert updateUi(-1) != null;
        disposables.add(
                OngoingCall.state
                        .subscribe(new Consumer<Integer>() {
                            @Override
                            public void accept(Integer integer) {
                                updateUi(integer);

                            }
                        }));

    }

    @SuppressLint({"SetTextI18n", "WrongConstant"})
    private Consumer<? super Integer> updateUi(Integer state) {


        if (CollectionsKt.listOf(new Integer[]{
                Call.STATE_DIALING,
                Call.STATE_RINGING,
                Call.STATE_ACTIVE,
                Call.STATE_NEW
        }).contains(state)) {

            if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    Activity#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.

            }
            Log.d("nitish", "" + state);
            if (state == Call.STATE_RINGING) {
                ongoingCall.answer();
                CallApp.getInstance().setCheckInComing(true);
                String number = getIntent().getData().getSchemeSpecificPart();
                if (number != null) {/*
                    incomingCallNumber = ;
                    System.out.println(incomingCallNumber);*/
                    CallApp.getInstance().setCandidateNumber(number.substring(3));
                }
                Log.d("nitish", "incoming");

            } else if (state == Call.STATE_ACTIVE && CallApp.getInstance().isCheckInComing() && OngoingCall.calls.size() == 1) {
                Uri uri = Uri.parse("tel:" + CommonPreference.getInstance(this).getHr());
                // Start call to the number in input
                startActivity(new Intent(Intent.ACTION_CALL, uri));
            } else if (OngoingCall.calls.size() == 1) {

                if (CallApp.getInstance().isCheckInComing()) {
                    Log.d("nitish", "no of call ==1 (iffffff)");
                } else {
                    Log.d("nitish", "call ==1 (else)");
                    Uri uri = Uri.parse("tel:" + "8182034498");
                    // Start call to the number in inHomput
                    startActivity(new Intent(Intent.ACTION_CALL, uri));
                }


            } else if (OngoingCall.calls.size() == 2) {
                ongoingCall.mergeCall();

            } else if (OngoingCall.calls.size() == 3) {

                if (!serviceCalled) {

                  //  audioManager.setSpeakerphoneOn(true);
                    audioManager.setMode(AudioManager.MODE_IN_CALL);

                   /* audioManager.setMode(AudioManager.ADJUST_RAISE);
                    audioManager.setStreamVolume(AudioManager.STREAM_VOICE_CALL,20,0);*/
                    serviceCalled = true;
                    Intent serviceIntent = new Intent(CallActivity.this, ForegroundServiceMy.class);
                    serviceIntent.putExtra("inputExtra", "Foreground Service Example in Android");
                    ContextCompat.startForegroundService(CallActivity.this, serviceIntent);
                }
            }
            callInfo.setText(asString(state));
        } else
            callInfo.setVisibility(View.VISIBLE);
        return null;
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    public void callDisconnted() {
        disposables.clear();
        Intent serviceIntent = new Intent(CallActivity.this, ForegroundServiceMy.class);
        stopService(serviceIntent);
        audioManager.setSpeakerphoneOn(false);
        if (!aswCalled) {
            if (!TextUtils.isEmpty(path1)) {

                finish();
            } else {
                Log.d("nitish", "   path empty");
                finish();
            }
        }

    }


    public static void start(Context context, Call call) {
        Intent intent = new Intent(context, CallActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .setData(call.getDetails().getHandle());
        context.startActivity(intent);
    }

}
