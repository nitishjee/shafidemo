package comp.shrofile.mycallingapp;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import java.io.IOException;


public class ForegroundServiceMy extends Service {
    private MediaRecorder recorder;
    public static final String CHANNEL_ID = "ForegroundServiceChannel";
    private String rec;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        String input = intent.getStringExtra("inputExtra");
        createNotificationChannel();
        Intent notificationIntent = new Intent(this, DialerActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Foreground Service")
                .setContentText(input)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentIntent(pendingIntent)
                .build();
        startForeground(1, notification);
        recorder = new MediaRecorder();
        //  recorder.reset();
        String time = new CommonMethods().getTIme();
        String path = new CommonMethods().getPath();
        if (TextUtils.isEmpty(CallApp.getInstance().getCallLogId())) {
            rec = path + "/" + CommonPreference.getInstance(CallApp.getInstance()).getHr() + "_" + CallApp.getInstance().getCandidateNumber() + "_" + time + ".mp4";
            CallActivity.path1 = rec;
            CallApp.getInstance().setKey(CommonPreference.getInstance(CallApp.getInstance()).getHr() + "_" + CallApp.getInstance().getCandidateNumber() + "_" + time + "..mp4");

        } else {
            rec = path + "/" + CallApp.getInstance().getCallLogId() + "";
            CallActivity.path1 = rec;
            CallApp.getInstance().setKey(CallApp.getInstance().getCallLogId() + "");
        }
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int v = audioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL);
        audioManager.setStreamVolume(AudioManager.STREAM_VOICE_CALL, v,1);
       // audioManager.setMicrophoneMute(false);
       // audioManager.setSpeakerphoneOn(true);
        audioManager.setMode(AudioManager.MODE_IN_CALL);

        recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_RECOGNITION);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(rec);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);


        //audioManager.setSpeakerphoneOn(true);
        Log.d("nitish", "onStartCommand: " + "Recording started");
        try {
            recorder.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        recorder.start();


        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        recorder.stop();
        recorder.reset();
        recorder.release();
        recorder = null;
        Log.d("nitish", "stop");
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            assert manager != null;
            manager.createNotificationChannel(serviceChannel);
        }
    }
}
