package comp.shrofile.mycallingapp;

import android.telecom.Call;
import android.telecom.VideoProfile;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.subjects.BehaviorSubject;

public class OngoingCall {
    public static BehaviorSubject<Integer> state = BehaviorSubject.create();
    public static List<Call> calls = new ArrayList<>();

    private Object callback = new Call.Callback() {
        @Override
        public void onStateChanged(Call call, int newState) {
            super.onStateChanged(call, newState);
            state.onNext(newState);
        }
    };

    public final void setCall(@Nullable Call value) {
      /*  if (call != null) {
            call.unregisterCallback((Call.Callback) callback);
        }*/

        if (value != null) {
            value.registerCallback((Call.Callback) callback);
            state.onNext(value.getState());
        }
        calls.add(value);

        
    }


    public void answer() {
        assert calls != null;
        calls.get(0).answer(VideoProfile.STATE_AUDIO_ONLY);
    }

    /* public void hangup() {
         assert call != null;
         call.disconnect();
     }

     public void hold() {
         call.hold();
     }
 */

    public void unHold() {
        calls.get(0).unhold();
    }

    void mergeCall() {
        calls.get(1).conference(calls.get(0));
    }

    public void disconnectAll() {
        for (int i = 0; i < calls.size(); i++) {
            calls.get(i).disconnect();
        }
        calls.clear();

    }

}
