package comp.shrofile.mycallingapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.PermissionChecker;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.telecom.TelecomManager;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import kotlin.collections.ArraysKt;

import static android.Manifest.permission.CALL_PHONE;
import static android.telecom.TelecomManager.ACTION_CHANGE_DEFAULT_DIALER;
import static android.telecom.TelecomManager.EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME;
import static androidx.core.content.PermissionChecker.PERMISSION_GRANTED;

public class DialerActivity extends AppCompatActivity {

    private EditText phoneNumberInput;
    private TextView tv_hr, tv_candidate;
    public static int REQUEST_PERMISSION = 0;
    Button btn_call;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialer);

        String notification = getIntent().getStringExtra("comeFrom");
        if (!TextUtils.isEmpty(notification)) {
            if (!CallApp.getInstance().getHrNumber().equals("")) {
                makeCall();
                return;
            }
        }
    }
    @SuppressLint("SetTextI18n")
    @Override
    protected void onStart() {
        super.onStart();
        offerReplacingDefaultDialer();
        String hr = CommonPreference.getInstance(this).getHr();
        tv_hr = findViewById(R.id.tv_hr);
        tv_candidate = findViewById(R.id.tv_candidate);
        btn_call = findViewById(R.id.btn_call);
        tv_hr.setText("Recruiter number" + hr);

        btn_call.setOnClickListener(view -> makeCall());
    }
    private void makeCall() {
        if (PermissionChecker.checkSelfPermission(this, CALL_PHONE) == PERMISSION_GRANTED) {
            Uri uri = Uri.parse("tel:" + CommonPreference.getInstance(this).getHr());
            startActivity(new Intent(Intent.ACTION_CALL, uri));
        } else {
            ActivityCompat.requestPermissions(this, new String[]{CALL_PHONE, Manifest.permission.READ_EXTERNAL_STORAGE
                    , Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO
                    , Manifest.permission.READ_PHONE_STATE, CALL_PHONE}, REQUEST_PERMISSION);
        }
    }
    private void offerReplacingDefaultDialer() {
        TelecomManager telecomManager = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            telecomManager = (TelecomManager) getSystemService(TELECOM_SERVICE);
        }
        assert telecomManager != null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!getPackageName().equals(telecomManager.getDefaultDialerPackage())) {
                Intent intent = new Intent(ACTION_CHANGE_DEFAULT_DIALER)
                        .putExtra(EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, getPackageName());
                startActivity(intent);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION && ArraysKt.contains(grantResults, PERMISSION_GRANTED)) {
            makeCall();
        }
    }

}
