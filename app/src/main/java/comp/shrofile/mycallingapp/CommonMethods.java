package comp.shrofile.mycallingapp;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Environment;
import android.provider.ContactsContract;
import android.telephony.SmsManager;
import android.util.Log;

import java.io.File;
import java.util.Calendar;

/*import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;*/

public class CommonMethods {

    final String TAGCM = "Inside Service";
    Calendar cal = Calendar.getInstance();

    public String getDate() {
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        int day = cal.get(Calendar.DATE);
        String date = String.valueOf(day) + "_" + String.valueOf(month) + "_" + String.valueOf(year);

        Log.d(TAGCM, "Date " + date);
        return date;
    }


    public String getTIme() {
        String am_pm = "";
        int sec = cal.get(Calendar.SECOND);
        int min = cal.get(Calendar.MINUTE);
        int hr = cal.get(Calendar.HOUR);
        if (hr == 0) {
            hr = 12;
        }
        int amPm = cal.get(Calendar.AM_PM);
        if (amPm == 1)
            am_pm = "PM";
        else if (amPm == 0)
            am_pm = "AM";

        String time = String.valueOf(hr) + ":" + String.valueOf(min) + ":" + String.valueOf(sec) + "_" + am_pm;

        Log.d(TAGCM, "Date " + time);
        return time;
    }

    public String getPath() {
        String internalFile = getDate();
        File file = new File(Environment.getExternalStorageDirectory() + "/My Call Recordings/");
        File file1 = new File(Environment.getExternalStorageDirectory() + "/My Call Recordings/" + internalFile + "/");
        if (!file.exists()) {
            file.mkdir();
        }
        if (!file1.exists())
            file1.mkdir();

        String path = file1.getAbsolutePath();
        Log.d(TAGCM, "Path " + path);

        return path;
    }

    public String getContactName(final String number, Context context) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};
        String contactName = "";
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                contactName = cursor.getString(0);
            }
            cursor.close();
        }

        if (contactName != null && !contactName.equals(""))
            return contactName;
        else
            return "";
    }


    // "bucketName":"shrofile-audios",
//     "folderName" : "uat-audios",   String url="https://shrofile-audios.s3.ap-south-1.amazonaws.com/"+folderName+"/"+key;
//                    System.out.println("success:"+url);

//    private void postRequest(String s3Url,String callLogId,String authRoleId,Context context) {
//        RequestQueue requestQueue= Volley.newRequestQueue(context);
//        JSONObject postparams = new JSONObject();
//        try{
//            postparams.put("callLogId", callLogId);
//            postparams.put("audioURL", s3Url);
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//        String url="http://api-uat.shrofile.com:8061/api/v1/callLog/updateCallLog";
//        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.POST, url, postparams,
//                response -> System.out.println(response.toString()),
//                error -> System.out.println("Error"))
//        {
//            @Override
//            public Map<String, String> getHeaders() {
//                Map<String,String > headers= new HashMap<>();
//                headers.put("Authorization",authRoleId);
//                headers.put("Accept","*/*");
//                headers.put("Content-Type","application/json");
//                return headers;
//            }
//        };
//        requestQueue.add(jsonObjectRequest);
//
//    }


    public static String getDuration(String path) {
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        mediaMetadataRetriever.setDataSource(path);
        String durationStr = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        return formateMilliSecond(Long.parseLong(durationStr));
    }

    public static String formateMilliSecond(long milliseconds) {
        String finalTimerString = "";
        String secondsString = "";

        // Convert total duration into time
        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);

        // Add hours if there
        if (hours > 0) {
            finalTimerString = hours + ":";
        }

        // Prepending 0 to seconds if it is one digit
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        return finalTimerString;
    }

    //function for sending text message

    public void sendSMSNow(String phoneNo, String message, Context context) {

        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";
        PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, new Intent(SENT), 0);
        PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0, new Intent(DELIVERED), 0);
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNo, null, message, sentPI, deliveredPI);

    }


    //retrofit update token function


}