package comp.shrofile.mycallingapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.telephony.SmsManager;

public class CallApp extends Application {

    @SuppressLint("StaticFieldLeak")
    private static CallApp callApp;
    private Activity activity;
    private String hr;
    private String candidate;
    private String key;
    private boolean checkInComing;
    private String fireBaseToken;
    private String hrNumber = "";
    private String candidateNumber = "";
    private String callLogId = "";
    private String authRoleId = "";
    private String bucketName = "";
    private String folderName = "";

    @Override
    public void onCreate() {
        super.onCreate();
        callApp = (CallApp) getApplicationContext();

    }

    public static CallApp getInstance() {
        return callApp;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public String getHr() {
        return hr;
    }

    public void setHr(String hr) {
        this.hr = hr;
    }

    public String getCandidate() {
        return candidate;
    }

    public void setCandidate(String candidate) {
        this.candidate = candidate;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getHrNumber() {
        return hrNumber;
    }

    public void setHrNumber(String hrNumber) {
        this.hrNumber = hrNumber;
    }

    public String getCandidateNumber() {
        return candidateNumber;
    }

    public void setCandidateNumber(String candidateNumber) {
        this.candidateNumber = candidateNumber;
    }

    public String getCallLogId() {
        return callLogId;
    }

    public void setCallLogId(String callLogId) {
        this.callLogId = callLogId;
    }

    public String getAuthRoleId() {
        return authRoleId;
    }

    public void setAuthRoleId(String authRoleId) {
        this.authRoleId = authRoleId;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }
    public boolean isCheckInComing() {
        return checkInComing;
    }

    public void setCheckInComing(boolean checkInComing) {
        this.checkInComing = checkInComing;
    }


    public String getFireBaseToken() {
        return fireBaseToken;
    }

    public void setFireBaseToken(String fireBaseToken) {
        this.fireBaseToken = fireBaseToken;
    }
    public void sendSMSNow(String phoneNo, String message, Context context) {

        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";
        PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, new Intent(SENT), 0);
        PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0, new Intent(DELIVERED), 0);
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNo, null, message, sentPI, deliveredPI);

    }
    public void openWhatsAppConversationUsingUri(Context context, String numberWithCountryCode, String message) {
        Uri uri = Uri.parse("https://api.whatsapp.com/send?phone=" +"+91"+ numberWithCountryCode + "&text=" + message);
        Intent sendIntent = new Intent(Intent.ACTION_VIEW, uri);
        sendIntent.setPackage("com.whatsapp");
        context.startActivity(sendIntent);
    }
}
