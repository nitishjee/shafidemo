package comp.shrofile.mycallingapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.PermissionChecker;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;

import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static androidx.core.content.PermissionChecker.PERMISSION_GRANTED;
import static comp.shrofile.mycallingapp.DialerActivity.REQUEST_PERMISSION;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText ed_hr, ed_candidate;
    // private TokenPresenter tokenPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String hr = CommonPreference.getInstance(this).getHr();
        if (!hr.equals("")) {
            Intent intent = new Intent(this, DialerActivity.class);
            startActivity(intent);
            finish();
            return;
        }


        Button btn = findViewById(R.id.btn);
        ed_hr = findViewById(R.id.ed_hr);
        // ed_candidate = findViewById(R.id.ed_candidate);
        btn.setOnClickListener(this);
        //   tokenPresenter = new TokenPresenter(this);
        ed_hr.setText(CallApp.getInstance().getHrNumber());
        //  ed_candidate.setText(CallApp.getInstance().getCandidateNumber());
        if (PermissionChecker.checkSelfPermission(this, READ_EXTERNAL_STORAGE) == PERMISSION_GRANTED) {
            recruiterVoicePath();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{CALL_PHONE, Manifest.permission.READ_EXTERNAL_STORAGE
                    , Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.SEND_SMS, Manifest.permission.RECORD_AUDIO
                    , Manifest.permission.READ_PHONE_STATE, CALL_PHONE}, REQUEST_PERMISSION);
        }
    }

    public String recruiterVoicePath() {
        File file = new File(Environment.getExternalStorageDirectory() + "/Recruiter/");
        if (!file.exists()) {
            file.mkdir();
        }
        String path = file.getAbsolutePath();
        return path;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn:
                if (!TextUtils.isEmpty(ed_hr.getText().toString())) {
                    recruiterVoicePath();
                    String token = CommonPreference.getInstance(this).getToken();
                    CommonPreference.getInstance(this).put(CommonPreference.Key.HR, ed_hr.getText().toString());
                    //tokenPresenter.uploadToken(ed_hr.getText().toString(), token);
                } else {
                    Toast.makeText(this, "Please fill the details first.", Toast.LENGTH_SHORT).show();
                }
                /*
                 */
                break;
        }


    }
}
